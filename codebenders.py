import sys

global N,M

def is_safe(board,i,j,v):
    if(board[i][j]!=0):
        return False
    if(v==1):
        if(i-1>=0 and j-1>=0 and board[i-1][j-1]>0):
            return False;
        if(i-1>=0 and j+1<M and board[i-1][j+1]>0):
            return False
        if(i+1<N and j-1>=0 and board[i+1][j-1]>0):
            return False
        if(i+1<N and j+1<M and board[i+1][j+1]>0):
            return False
        
    elif(v==2):
        for a in range(M):
            if(a!=j and board[i][a]>0):
                return False
            
        for a in range(N):
            if(a!=i and board[a][j]>0):
                return False
            
    elif(v==3):
        x=max(N,M)
        for a in range(1,x):
            if(i-a<0 or j-a<0):
                break
            if(board[i-a][j-a]>0):
                return False
            
        for a in range(1,x):
            if(i+a>=N or j+a>=M):
                break
            if(board[i+a][j+a]>0):
                return False
            
        for a in range(1,x):
            if(i+a>=N or j-a<0):
                break
            if(board[i+a][j-a]>0):
                return False
            
        for a in range(1,x):
            if(i-a<0 or j+a>=M):
                break
            if(board[i-a][j+a]>0):
                return False
        
    elif(v==4):
        if(i-2>=0 and j+1<M and board[i-2][j+1]>0):
            return False
        if(i-2>=0 and j-1>=0 and board[i-2][j-1]>0):
            return False
        if(i-1>=0 and j+2<M and board[i-1][j+2]>0):
            return False
        if(i+1<N and j+2<M and board[i+1][j+2]>0):
            return False
        if(i-1>=0 and j-2>=0 and board[i-1][j-2]>0):
            return False
        if(i+1<N and j-2>=0 and board[i+1][j-2]>0):
            return False
        if(i+2<N and j+1<M and board[i+2][j+1]>0):
            return False
        if(i+2<N and j-1>=0 and board[i+2][j-1]>0):
            return False
        
    elif(v==5):
        if(is_safe(board,i,j,3)==False or is_safe(board,i,j,2)==False):
            return False
        
    else:
        if(i-1>=0 and j-1>=0 and board[i-1][j-1]>0):
            return False
        if(i-1>=0 and board[i-1][j]>0):
            return False
        if(i-1>=0 and j+1<M and board[i-1][j+1]>0):
            return False
        if(j-1>=0 and board[i][j-1]>0):
            return False
        if(j+1<M and board[i][j+1]>0):
            return False
        if(i+1<N and j-1>=0 and board[i+1][j-1]>0):
            return False
        if(i+1<N and board[i+1][j]>0):
            return False
        if(i+1<N and j+1<M and board[i+1][j+1]>0):
            return False

    return True

def fill_util(board,i,j,v):
    if(v==1):
        if(i-1>=0 and j-1>=0):
            board[i-1][j-1]=-1
        if(i-1>=0 and j+1<M):
            board[i-1][j+1]=-1
        if(i+1<N and j-1>=0):
            board[i+1][j-1]=-1
        if(i+1<N and j+1<M):
            board[i+1][j+1]=-1
    
    elif(v==2):
        for a in range(M):
            if(a!=j):
                board[i][a]=-1
            
        for a in range(N):
            if(a!=i):
                board[a][j]=-1
            
    elif(v==3):
        x=max(N,M)
        for a in range(1,x):
            if(i-a<0 or j-a<0):
                break
            board[i-a][j-a]=-1
            
        for a in range(1,x):
            if(i+a>=N or j+a>=M):
                break
            board[i+a][j+a]=-1
            
        for a in range(1,x):
            if(i+a>=N or j-a<0):
                break
            board[i+a][j-a]=-1
            
        for a in range(1,x):
            if(i-a<0 or j+a>=M):
                break
            board[i-a][j+a]=-1
        
    elif(v==4):
        if(i-2>=0 and j+1<M):
            board[i-2][j+1]=-1
        if(i-2>=0 and j-1>=0):
            board[i-2][j-1]=-1
        if(i-1>=0 and j+2<M):
            board[i-1][j+2]=-1
        if(i+1<N and j+2<M):
            board[i+1][j+2]=-1
        if(i-1>=0 and j-2>=0):
            board[i-1][j-2]=-1
        if(i+1<N and j-2>=0):
            board[i+1][j-2]=-1
        if(i+2<N and j+1<M):
            board[i+2][j+1]=-1
        if(i+2<N and j-1>=0):
            board[i+2][j-1]=-1
        
    elif(v==5):
        fill_util(board,i,j,2)
        fill_util(board,i,j,3)
        
    else:
        if(i-1>=0 and j-1>=0):
            board[i-1][j-1]=-1
        if(i-1>=0):
            board[i-1][j]=-1
        if(i-1>=0 and j+1<M):
            board[i-1][j+1]=-1
        if(j-1>=0):
            board[i][j-1]=-1
        if(j+1<M):
            board[i][j+1]=-1
        if(i+1<N and j-1>=0):
            board[i+1][j-1]=-1
        if(i+1<N):
            board[i+1][j]=-1
        if(i+1<N and j+1<M):
            board[i+1][j+1]=-1

    return True

def unfill_util(board,copy):
    for i in range(N):
        for j in range(M):
            board[i][j]=copy[i][j]
            
def solve_util(board,ar,index): 
    if index>=len(ar):
        return True
    for i in range(N): 
        for j in range(M):
            if is_safe(board,i,j,ar[index])==True:
                copy=[]
                for k in range(N):
                    copy.append(list())
                    for l in range(M):
                        copy[k].append(board[k][l])
                #print(board)
                board[i][j]=ar[index]
                fill_util(board,i,j,ar[index])
                if solve_util(board,ar,index+1)==True: 
                    return True
                board[i][j]=0
                unfill_util(board,copy)
    return False

def solve():
    global N,M
    
    N=8;
    M=8;
    
    
    board=list()
    for i in range(N):
        board.append(list())
        for j in range(M):
            board[i].append(0)
    
    ar=[]
    
    
    n=int(sys.argv[1])
    for i in range(n):
        ar.append(1)
        
    n=int(sys.argv[2])
    for i in range(n):
        ar.append(2)
    
    n=int(sys.argv[3])
    for i in range(n):
        ar.append(3)
    
    n=int(sys.argv[4])
    for i in range(n):
        ar.append(4)
    
    n=int(sys.argv[5])
    for i in range(n):
        ar.append(5)
    
    n=int(sys.argv[6])
    for i in range(n):
        ar.append(6)
    
    
    if solve_util(board,ar,0)==False: 
        print("Solution does not exist")
        return False
    for i in range(N): 
        for j in range(M): 
            if(board[i][j]==-1):
                print("0",end=" ")
            else:
                print(board[i][j],end=" ") 
        print()
    return True
solve() 
